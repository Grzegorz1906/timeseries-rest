# QA task

This is a simple QA task project using Serenity with Cucumber and REST Assured

## Installation:

1.After cloning repo please run in main folder:

`mvn clean install`

2.In terminal export your API key using following command:

`export API_KEY=your_api_key_here`

3.Happy testing:)

## Run all tests:

`mvn clean verify`

## Run tests using tags:

`mvn clean verify -Dcucumber.filter.tags="@Performance"`

## Additional report after finished tests:

`target/site/serenity/index.html`
