Feature: GET /timeseries endpoint

  @Timeseries @HappyPath @Performance
  Scenario: Retrieving historical rates within a specified date range
    When I make a GET request with start date "2012-05-01" and end date "2012-05-03"
    Then the response status code should be 200
    And the response body should have "base" as "EUR"
    And the response body should have "start_date" as "2012-05-01"
    And the response body should have "end_date" as "2012-05-03"
    And the response body should have "timeseries" as true
    And the response body should have "success" as true
    And the response body should contain rates for all supported currencies
    And API response time is within 3 seconds from current time

  # sometimes I wait longer and I get error 524 in response (timeout occurred) and google made in past statistics, that
  # response time should be shorter than 3 seconds for new users (if user didn't visit some page in past) because they
  # loose trust quickly after, so I added such "mini performance test" to notify devops if this issue occurred in future
  # (of course this time can be bigger on test environment, but API from this site is on prod)

  @Timeseries
  Scenario: Retrieving historical rates within a specified date range with base and symbols
    When I make a GET request with start date "2012-05-01", end date "2012-05-02", base currency as "USD", and symbols "AUD,CAD"
    Then the response status code should be 200
    And the response body should have "success" as true
    And the response body should have "timeseries" as true
    And the response body should have "start_date" as "2012-05-01"
    And the response body should have "end_date" as "2012-05-02"
    And the response body should have "base" as "USD"
    And the response body should contain rates only for "AUD,CAD"
    And the response body should have rates for "AUD" as 0.966101 on "2012-05-01"
    And the response body should have rates for "CAD" as 0.984437 on "2012-05-01"
    And the response body should have rates for "AUD" as 0.968926 on "2012-05-02"
    And the response body should have rates for "CAD" as 0.987846 on "2012-05-02"

  # Rates values part in this test could be also parametrized with table, but because this API key is for 100 requests
  # only, I needed to save requests to have enough free requests to make other tests:) In Cucumber, each scenario outline
  # row represents an independent scenario execution. Therefore, in this scenario using outline, Cucumber will execute
  # the scenario four times, once for each row in the examples table. Each execution of the scenario will result in
  # a separate GET request being made, because Cucumber treats each scenario execution as distinct.