Feature: GET /timeseries endpoint error handling

  @Timeseries @ErrorHandling
  Scenario: Retrieving historical rates with an invalid date range (end date before start date)
    When I make a GET request with an invalid date range (end date before start date)
    Then the response status code should be 200
    And the response body should have "success" as false
    And the response body should have "error.code" as 504
    And the response body should have "error.type" as "invalid_time_frame"
    And the response body should contain an error message indicating invalid time frame

  @Timeseries @ErrorHandling
  Scenario: Retrieving historical rates with an invalid date format - fixed, yesterday was error 500:)
    When I make a GET request with an invalid date format
    Then the response status code should be 200
    And the response body should have "success" as false
    And the response body should have "error.code" as 502
    And the response body should have "error.type" as "invalid_start_date"
    And the response body should contain an error message indicating invalid start date format

  @Timeseries @ErrorHandling
  Scenario: Retrieving historical rates with invalid base currency
    When I make a GET request with an invalid base currency
    Then the response status code should be 200
    And the response body should have "success" as false
    And the response body should have "error.code" as 201
    And the response body should have "error.type" as "invalid_base_currency"

  @Timeseries @ErrorHandling
  Scenario: Retrieving historical rates with invalid symbols
    When I make a GET request with invalid symbols
    Then the response status code should be 200
    And the response body should have "success" as false
    And the response body should have "error.code" as 202
    And the response body should have "error.type" as "invalid_currency_codes"
    And the response body should contain an error message indicating invalid currency codes

  @Timeseries @ErrorHandling
  Scenario: request with missing start date
    When I make a GET request with missing start date
    Then the response status code should be 200
    And the response body should have "success" as false
    And the response body should have "error.code" as 501
    And the response body should have "error.type" as "no_timeframe_supplied"
    And the response body should contain an error message indicating missing time frame

  @Timeseries @ErrorHandling
  Scenario: request with missing end date
    When I make a GET request with missing end date
    Then the response status code should be 200
    And the response body should have "success" as false
    And the response body should have "error.code" as 501
    And the response body should have "error.type" as "no_timeframe_supplied"
    And the response body should contain an error message indicating missing time frame

  @Timeseries @ErrorHandling
  Scenario: request with with not existing API key
    When I make a GET request with not existing API key
    Then the response status code should be 401
    And the response body should have "message" as "Invalid authentication credentials"

  @Timeseries @ErrorHandling
  Scenario: request with without API key
    When I make a GET request without API key
    Then the response status code should be 401
    And the response body should have "message" as "No API key found in request"

  @Timeseries @ErrorHandling
  Scenario: Invalid endpoint - this test should fail (bug - incorrect error handling)
    When I make a GET request using invalid endpoint name
    Then the response status code should be 404
    And the response body should have "success" as false
    And the response body should contain an error message indicating invalid endpoint

  @Timeseries @ErrorHandling
  Scenario: Invalid URL before endpoint name
    When I make a GET request using invalid URL before endpoint name
    Then the response status code should be 404
    And the response body should have "message" as "no Route matched with those values"

  @Timeseries @ErrorHandling
  Scenario: API request limit exceeded - do not run until you want to reach monthly limit:)
    Given I make 101 GET requests to reach limit in free account
    When I make a GET request with start date "2012-05-01" and end date "2012-05-03"
    Then the response status code should be 429
    And the response body should have "message" as "Too many requests	API request limit exceeded. See section Rate Limiting for more info."

  @Timeseries @ErrorHandling
  Scenario: unsupported http method
    When I make request with unsupported Delete method
    Then the response status code should be 404
    And the response body should have "message" as "no Route matched with those values"