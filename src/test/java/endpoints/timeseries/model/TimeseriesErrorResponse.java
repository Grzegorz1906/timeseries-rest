package endpoints.timeseries.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TimeseriesErrorResponse {

    @JsonProperty("success")
    private boolean success;

    @JsonProperty("error")
    private ErrorDetails error;

    @Data
    public static class ErrorDetails {
        @JsonProperty("code")
        private int code;

        @JsonProperty("type")
        private String type;

        @JsonProperty("info")
        private String info;
    }
}
