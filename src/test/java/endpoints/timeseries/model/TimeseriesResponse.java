package endpoints.timeseries.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class TimeseriesResponse {

    @JsonProperty("success")
    private boolean success;

    @JsonProperty("timeseries")
    private boolean timeseries;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("base")
    private String base;

    @JsonProperty("rates")
    private Map<String, Map<String, Double>> rates;
}
