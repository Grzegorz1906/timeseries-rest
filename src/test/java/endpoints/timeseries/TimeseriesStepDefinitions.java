package endpoints.timeseries;

import endpoints.timeseries.model.Currency;
import endpoints.timeseries.model.TimeseriesErrorResponse;
import endpoints.timeseries.model.TimeseriesResponse;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TimeseriesStepDefinitions {

    private static final String API_KEY = System.getenv("API_KEY"); //for safety take as env variable
    private static final String ENDPOINT = "https://api.apilayer.com/fixer/timeseries";
    private static final String INVALID_ENDPOINT = "https://api.apilayer.com/fixer/invalid_endpoint";
    private static final String INVALID_URL = "https://api.apilayer.com/timeseries";
    private Response response;
    private TimeseriesResponse timeseriesResponse;
    private TimeseriesErrorResponse timeseriesErrorResponse;

    @When("I make a GET request with start date {string} and end date {string}")
    public void iMakeAGETRequestWithStartDateAndEndDate(String startDate, String endDate) {
        response = given()
                .queryParam("start_date", startDate)
                .queryParam("end_date", endDate)
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesResponse = response.as(TimeseriesResponse.class);
    }

    @When("I make a GET request with not existing API key")
    public void iMakeAGETRequestWithNotExistingAPIkey() {
        response = given()
                .queryParam("start_date", "2012-05-01")
                .queryParam("end_date", "2012-05-03")
                .header("apikey", "NOT_EXISTING_API_KEY_SHOULD_GIVE_401")
                .when()
                .get(ENDPOINT);
    }

    @And("the response body should contain rates for {string}")
    public void theResponseBodyShouldContainRatesFor(String currencies) {
        assertNotNull("Rates are null", timeseriesResponse.getRates());
        String[] currencyList = currencies.split(",");
        Map<String, Map<String, Double>> rates = timeseriesResponse.getRates();

        rates.keySet().forEach(date -> {
            Map<String, Double> currenciesForDate = rates.get(date);
            for (String currency : currencyList) {
                assertTrue("Currency " + currency + " not found in rates", currenciesForDate.containsKey(currency));
            }
        });
    }

    @And("the response body should contain rates for all supported currencies")
    public void theResponseBodyShouldContainRatesForAllSupportedCurrencies() {
        assertNotNull("Rates are null", timeseriesResponse.getRates());
        Currency[] currencyList = Currency.values();
        Map<String, Map<String, Double>> rates = timeseriesResponse.getRates();

        rates.keySet().forEach(date -> {
            Map<String, Double> currenciesForSingleDate = rates.get(date);
            for (Currency currency : currencyList) {
                assertTrue("Currency " + currency + " not found in rates", currenciesForSingleDate.containsKey(currency.name()));
            }
        });
    }

    @And("the response body should not contain rates for other currencies")
    public void theResponseBodyShouldNotContainRatesForOtherCurrencies() {
        assertEquals("Unexpected number of currencies", 3, timeseriesResponse.getRates().size());
        assertTrue("Rates for other currencies are present", timeseriesResponse.getRates().containsKey("AUD"));
        assertTrue("Rates for other currencies are present", timeseriesResponse.getRates().containsKey("CAD"));
        assertTrue("Rates for other currencies are present", timeseriesResponse.getRates().containsKey("USD"));
    }

    @When("I make a GET request with start date {string}, end date {string}, base currency as {string}, and symbols {string}")
    public void iMakeAGETRequestWithStartDateEndDateBaseCurrencyAsAndSymbols(String startDate, String endDate, String baseCurrency, String symbols) {
        response = given()
                .queryParam("start_date", startDate)
                .queryParam("end_date", endDate)
                .queryParam("base", baseCurrency)
                .queryParam("symbols", symbols)
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesResponse = response.as(TimeseriesResponse.class);
    }

    @And("the response body should contain rates only for {string}")
    public void theResponseBodyShouldContainRatesOnlyFor(String currencies) {
        List<String> currencyList = Arrays.asList(currencies.split(","));
        Map<String, Map<String, Double>> rates = timeseriesResponse.getRates();
        assertFalse("Rates are empty", rates.isEmpty());

        for (Map<String, Double> ratesForDate : rates.values()) {
            for (String currency : currencyList) {
                assertThat("Currency " + currency + " is not present", ratesForDate, hasKey(currency));
            }
            assertThat("Unexpected currencies are present", ratesForDate.keySet(), containsInAnyOrder(currencyList.toArray()));
        }
    }

    @When("I make a GET request with an invalid date range \\(end date before start date)")
    public void iMakeAGETRequestWithAnInvalidDateRangeEndDateBeforeStartDate() {
        response = given()
                .queryParam("start_date", "2012-05-03")
                .queryParam("end_date", "2012-05-01")
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @When("I make a GET request with an invalid date format")
    public void iMakeAGETRequestWithAnInvalidDateFormat() {
        response = given()
                .queryParam("start_date", "aaa-05-03")
                .queryParam("end_date", "2012-05-1")
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @When("I make a GET request with an invalid base currency")
    public void iMakeAGETRequestWithAnInvalidBaseCurrency() {
        response = given()
                .queryParam("start_date", "2012-05-01")
                .queryParam("end_date", "2012-05-03")
                .queryParam("base", "INVALID")
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @When("I make a GET request with invalid symbols")
    public void iMakeAGETRequestWithInvalidSymbols() {
        response = given()
                .queryParam("start_date", "2012-05-01")
                .queryParam("end_date", "2012-05-03")
                .queryParam("symbols", "INVALID")
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @Then("the response body should contain an error message indicating invalid date range")
    public void theResponseBodyShouldContainAnErrorMessageIndicatingInvalidDateRange() {
        assertTrue("Invalid date range error not found", timeseriesErrorResponse.getError().getInfo().contains("Invalid date range"));
    }

    @Then("the response body should have {string} as false")
    public void theResponseBodyShouldHaveFalseValue(String key) {
        response.then().body(key, is(false));
    }

    @And("the response body should have {string} as {int}")
    public void theResponseBodyShouldHaveKeyValue(String key, int value) {
        response.then().body(key, is(value));
    }

    @And("the response body should contain an error message indicating invalid currency codes")
    public void theResponseBodyShouldContainAnErrorMessageIndicatingInvalidCurrencyCodes() {
        String expectedMessage = "You have provided one or more invalid Currency Codes";
        assertThat("Error message indicating invalid currency codes not found",
                timeseriesErrorResponse.getError().getInfo(), containsString(expectedMessage));
    }

    @And("the response body should contain an error message indicating invalid time frame")
    public void theResponseBodyShouldContainAnErrorMessageIndicatingInvalidTimeFrame() {
        String expectedMessage = "You have entered an invalid Time-Frame. [Required format: ...&start_date=YYYY-MM-DD&end_date=YYYY-MM-DD]";
        assertThat("Error message indicating invalid time frame not found",
                timeseriesErrorResponse.getError().getInfo(), containsString(expectedMessage));
    }

    @And("the response body should contain an error message indicating invalid date format")
    public void theResponseBodyShouldContainAnErrorMessageIndicatingInvalidDateFormat() {
        String expectedMessage = "You have entered an invalid date format. [Required format: ...&start_date=YYYY-MM-DD&end_date=YYYY-MM-DD]";
        assertThat("Error message indicating invalid date format not found",
                timeseriesErrorResponse.getError().getInfo(), containsString(expectedMessage));
    }

    @And("the response body should contain an error message indicating invalid start date format")
    public void theResponseBodyShouldContainAnErrorMessageIndicatingInvalidStartDateFormat() {
        String expectedMessage = "You have specified an invalid start date. " +
                "Please try again or refer to the API documentation available at https://serpstack.com/documentation.";
        assertThat("Error message indicating invalid date format not found",
                timeseriesErrorResponse.getError().getInfo(), containsString(expectedMessage));
    }

    @Then("the response body should have rates for {string} as {double} on {string}")
    public void theResponseBodyShouldHaveRatesForCurrencyAsOn(String currency, double rate, String date) {
        assertEquals(rate, timeseriesResponse.getRates().get(date).get(currency), 0.000001);
    }

    @Then("API response time is within {int} seconds from current time")
    public void apiResponseTimeIsWithinSecondsFromCurrentTime(int maxSeconds) {
        String dateString = response.getHeader("Date");
        ZonedDateTime responseTime = ZonedDateTime.parse(dateString, DateTimeFormatter.RFC_1123_DATE_TIME);
        ZonedDateTime currentTime = Instant.now().atZone(responseTime.getZone());
        long timeDifferenceInSeconds = currentTime.toEpochSecond() - responseTime.toEpochSecond();
        assertTrue("API response time is more than " + maxSeconds + " seconds from current time",
                timeDifferenceInSeconds <= maxSeconds);
    }

    @When("I make a GET request with missing start date")
    public void makeRequestWithMissingStartDate() {

        response = given()
                .queryParam("end_date", "2012-05-03")
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @When("I make a GET request with missing end date")
    public void makeRequestWithMissingEndDate() {

        response = given()
                .queryParam("start_date", "2012-05-01")
                .header("apikey", API_KEY)
                .when()
                .get(ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @And("the response body should contain an error message indicating missing time frame")
    public void verifyErrorMessageForMissingTimeFrame() {
        assertTrue("Missing time frame error not found",
                timeseriesErrorResponse.getError().getInfo().contains("You have not specified a Time-Frame. " +
                        "[Required format: ...&start_date=YYYY-MM-DD&end_date=YYYY-MM-DD]"));
    }

    @When("I make a GET request without API key")
    public void makeRequestWithoutAPIkey() {
        response = given()
                .when()
                .queryParam("start_date", "2012-05-01")
                .queryParam("end_date", "2012-05-03")
                .get(INVALID_ENDPOINT);
    }

    @When("I make a GET request using invalid endpoint name")
    public void makeRequestWithInvalidEndpoint() {
        response = given()
                .when()
                .header("apikey", API_KEY)
                .queryParam("start_date", "2012-05-01")
                .queryParam("end_date", "2012-05-03")
                .get(INVALID_ENDPOINT);
        timeseriesErrorResponse = response.as(TimeseriesErrorResponse.class);
    }

    @When("I make a GET request using invalid URL before endpoint name")
    public void makeRequestWithInvalidUrl() {
        response = given()
                .when()
                .header("apikey", API_KEY)
                .queryParam("start_date", "2012-05-01")
                .queryParam("end_date", "2012-05-03")
                .get(INVALID_URL);
    }

    @Then("the response status code should be {int}")
    public void theResponseStatusCodeShouldBe(int expectedStatusCode) {
        response.then().statusCode(expectedStatusCode);
    }

    @And("the response body should have {string} as {string}")
    public void theResponseBodyShouldHaveKeyValue(String key, String value) {
        response.then().body(key, is(value));
    }

    @And("the response body should have {string} as true")
    public void theResponseBodyShouldHaveTrueValue(String key) {
        response.then().body(key, is(true));
    }

    @And("the response body should contain an error message indicating invalid endpoint")
    public void verifyErrorMessageForInvalidEndpoint() {
        assertTrue("Invalid endpoint error not found", timeseriesErrorResponse.getError().getInfo().toLowerCase().contains("invalid endpoint"));
    }

    @Given("I make {int} GET requests to reach limit in free account")
    public void iMakeGETRequestsToReachLimitInFreeAccount(int requestsNumber) {
        for (int i = 0; i < requestsNumber; i++) {
            response = given()
                    .queryParam("start_date", "2012-05-01")
                    .queryParam("end_date", "2012-05-03")
//                    .header("apikey", API_KEY) //do not run until you want to reach monthly limit:)
                    .when()
                    .get(ENDPOINT);
        }
    }

    @When("I make request with unsupported Delete method")
    public void makeRequestWithUnsupportedDeleteMethod() {
        response = given()
                .header("apikey", API_KEY)
                .when()
                .delete(ENDPOINT);
    }
}
